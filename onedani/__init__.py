"""
This library creates a simple 1D visualization animation to help students
understand how a temperature will evolve within a simple wall conduction
problem.
"""

__version__ = "0.1.0"
